import cv
import math
import numpy as np

def blur(img):
    # make kernel
    size = 7
    kernel = np.zeros((size, size))
    for i in range(size / 2 + 1):
        kernel[i][i] = math.exp(i)
    kernel /= np.sum(kernel)
    kernel = cv.fromarray(kernel)

    # show kernel
    kernel_big = cv.CreateMat(img.cols, img.rows, kernel.type)
    cv.Resize(kernel, kernel_big)
    cv.NamedWindow('blur kernel', cv.CV_WINDOW_AUTOSIZE)
    cv.MoveWindow('blur kernel', 500, 500)
    cv.ShowImage('blur kernel', kernel_big)

    # apply filter
    img32 = cv.CreateMat(img.rows, img.cols, cv.CV_32FC1)
    cv.ConvertScale(img, img32, 1.0 / 256.0)
    cv.Filter2D(img32, img32, kernel)

    # back to 8 bits
    img8 = cv.CreateMat(img.rows, img.cols, cv.CV_8UC1)
    cv.ConvertScale(img32, img8, 256.0)
    
    return img8, kernel
